import java.util.*;
import java.lang.String;

public class MyBC {

    private int priority(char c){
        if(c == '*' || c== '/')
            return 2;
        else if(c == '+' || c== '-')
            return 1;
        else
            return 0;
    }

    private boolean leftPirority(Character c1,char c2){
        if(c1 == null)
            return false;
        return priority(c1)>=priority(c2);
    }
    private boolean isNumber(char c){
        if('0' <= c && c<='9')
            return true;
        return false;
    }
    private boolean isLeftBracket(char c){
        if(c == '(')
            return true;
        return false;
    }
    private boolean isRightBracket(char c){
        if(c == ')')
            return true;
        return false;
    }
    private boolean isOperator(char c){
        if(c == '+' || c == '-' || c == '*' ||c == '/')
            return true;
        return false;
    }


    public String parse(String str){
        class Stack<S>{
            private LinkedList<S> list = new LinkedList<S>();

            public void push(S t){
                list.addLast(t);
            }

            public S pop(){
                return list.removeLast();
            }

            public S top(){
                return list.peekLast();
            }

            public boolean isEmpty(){
                return list.isEmpty();
            }
        }

        Stack<Character> stack = new Stack<>();
        StringBuilder string = new StringBuilder();
        char[] cs = str.toCharArray();

        for(int i=0; i<cs.length; ++i){
            char c = cs[i];
            if( isNumber(c)){
                string.append(c);

            }else if( isLeftBracket(c)){
                stack.push(c);

            }else if( isOperator(c)){

                while( leftPirority(stack.top(), c)){
                    string.append( stack.pop());
                }
                stack.push(c);

            }else if( isRightBracket(c)){
                while( !isLeftBracket( stack.top())){
                    string.append(stack.pop());
                }
                stack.pop();
            }
        }

        while( !stack.isEmpty()){
            string.append(stack.pop());
        }

        return string.toString();
    }



}
