import java.util.Scanner;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;
public class sizeyunsuan {
    public static void main(String[] args){
        Scanner read = new Scanner(System.in);
        BufferedReader reader = null;
        InputStreamReader inputStreamReader= null ;
        try{
            System.out.println("请输入算式,按回车进行输出：\n");
            inputStreamReader = new InputStreamReader(System.in);
            reader = new BufferedReader(inputStreamReader);
            String str = reader.readLine();
            boolean flag = true;
            while(flag==true){
                System.out.println("结果为："+jisuan(str));
                System.out.println("是否还要继续？？？(不想继续就输'0'，然后回车；想继续就输其他的数字）");
                int x = read.nextInt();
                if(x==0){
                    flag=false;
                    break;
                }
                System.out.println("请输入算式,按回车进行输出：\n");
                str = reader.readLine();
            }
        }catch(Exception adp){
            adp.printStackTrace();
        }
        if(reader!=null){
            try {
                reader.close();
            } catch (IOException atp) {
                atp.printStackTrace();
            }
        }
    }

    public static float jisuan(String s) throws Exception{
        if(s == null || "".equals(s.trim())) {
            return 0;
        }
        int a1=s.indexOf("+");
        int a2=s.indexOf("-");
        int a3=s.indexOf("*");
        int a4=s.indexOf("/");
        int a5=s.indexOf("(");
        if(a1==-1&&a2==-1&&a3==-1&&a4==-1){
            if(s.trim()==null||"".equals(s.trim())){
                throw new Exception("运行错误！！！");
            }
            return Float.parseFloat(s.trim());
        }

        if(a5!=-1){
            int a6=s.indexOf(")");
            if(a6==-1){
                throw new Exception("请检查括号！！！");
            }else{
                float f=jisuan(s.substring(a5+1,a6).trim());
                s=s.replace(s.substring(a5,a6+1), String.valueOf(f));
                return jisuan(s);
            }
        }

        if(a1!=-1){
            return jisuan(s.substring(0,a1))+jisuan(s.substring(a1+1,s.length()));
        }
        if(a2!=-1){
            return jisuan(s.substring(0,a2))-jisuan(s.substring(a2+1,s.length()));
        }
        if(a3!=-1){
            return jisuan(s.substring(0,a3))*jisuan(s.substring(a3+1,s.length()));
        }
        if(a4!=-1){
            return jisuan(s.substring(0,a4))/jisuan(s.substring(a4+1,s.length()));
        }
        return Integer.parseInt(s.trim());
    }
}
