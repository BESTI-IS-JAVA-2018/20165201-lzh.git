import java.util.*;
import java.lang.*;
class PC {
    CPU cpu;
    HardDisk HD;
    PC() {
        System.out.println("成功创建PC对象 ！！！");
    }
    PC(HardDisk hd) { }
    PC(CPU cpu) { }
    PC(HardDisk hd,CPU cpu) {}
    void setCPU(CPU c) {
        cpu=c;
    }
    void setHardDisk(HardDisk h) {
        HD = h;
    }
    void show(){
        System.out.println("CPU的速度："+cpu.speed);
        System.out.println("硬盘容量："+HD.amount);
    }
    public String toString(){
        return "重写PC类的toString()方法 ！";
    }
    public boolean equals(String s) {
        if(s=="高级计算机 ！"){
            return true;
        }
        else {
            return false;
        }
    }

}
class CPU {
    int speed;
    CPU() {
        System.out.println("成功创建一个CPU对象 ！！！");
    }
    void setSpeed(int s) {
        speed=s;
    }
    CPU(HardDisk hd) {}
    CPU(PC pc){}
    CPU(HardDisk hd,PC pc){}
    int getSpeed() {
        return speed;
    }
    public String toString(){
        return "重写CPU类的toString()方法 ！";
    }
    public boolean equals(String s) {
        if(s=="超快cpu"){
            return true;
        }
        else {
            return false;
        }
    }
}
class HardDisk {
    int amount;
    HardDisk() {
        System.out.println("成功创建HardDisk对象 ！！！");
    }
    HardDisk(PC pc) {}
    HardDisk(CPU cpu) {}
    HardDisk(PC pc,CPU cpu) {}
    void setAmount(int a) {
        amount = a;
    }
    int getAmount() {
        return amount;
    }
    public String toString(){
        return "重写HardDisk类的toString()方法 ！";
    }
    public boolean equals(String s) {
        if(s=="超大内存 ！"){
            return true;
        }
        else {
            return false;
        }
    }
}
public class Test {
    public static void main(String args[]) {
        CPU cpu=new CPU();
        PC pc=new PC();
        HardDisk disk = new HardDisk();
        System.out.println(pc.toString());
        System.out.println(cpu.toString());
        System.out.println(disk.toString());
        System.out.println("CPU是快速CPU吗 ？ "+cpu.equals("快速CPU ！"));
        System.out.println("硬盘是大容量的吗 ？ "+disk.equals("不是，而是小容量 ！"));
        System.out.println("计算机是高级计算机吗 ？ "+pc.equals("高级计算机 ！"));
        cpu.setSpeed(2200);
        disk.setAmount(200);
        pc.setCPU(cpu);
        pc.setHardDisk(disk);
        pc.show();
    }
}
