import java.util.*;

class Student implements Comparable {
    int id;
    String name;

    Student(int i, String n) {
        id = i;
        name = n;
    }

    public int compareTo(Object b) {
        Student stu = (Student) b;
        return (this.id - stu.id);
    }
}

public class MyList {
    public static void main(String[] args) {
        //选用合适的构造方法，用你学号前后各两名同学的学号创建四个结点
        LinkedList<Student> list = new LinkedList<>();
        list.add(new Student(20165238, "步繁"));
        list.add(new Student(20165239, "其米仁增"));
        list.add(new Student(20165202, "贾海粟"));
        list.add(new Student(20165203, "夏云霄"));
        //把上面四个节点连成一个没有头结点的单链表
        Iterator<Student> iter = list.iterator();
        //遍历单链表，打印每个结点的
        System.out.println("打印初始的单链表：");
        while (iter.hasNext()) {
            Student stu = iter.next();
            System.out.println(stu.id + " " + stu.name);
        }
        //把你自己插入到合适的位置（学号升序）
        list.add(new Student(20165201, "李梓豪"));
        Collections.sort(list);
        //遍历单链表，打印每个结点的
        iter = list.iterator();
        System.out.println("打印插入自己的学号和姓名并排序后的单链表：");
        while (iter.hasNext()) {
            Student stu = iter.next();
            System.out.println(stu.id + "  " + stu.name);
        }
        //从链表中删除自己
        list.remove(0);
        iter = list.iterator();
        //遍历单链表，打印每个结点的
        System.out.println("打印删除自己的学号和姓名并排序后的单链表：");
        while (iter.hasNext()) {
            Student stu = iter.next();
            System.out.println(stu.id + "  " + stu.name);
        }
    }
}
