public class Rational {
  int numberator=1;
  int denominator=1;
  void setNumberator(int a) {
    int c=f(Math.abs(a),denominator);
    numberator=a/c;
    denominator=denominator/c;
    if(numberator<0 && denominator<0) {
      numberator=-numberator;
      denominator=-denominator;
    }
  }
  void setDenominator(int b) {
    int c=f(numberator,Math.abs(b));
    numberator=numberator/c;
    denominator=b/c;
    if(numberator<0 && denominator<0){
      numberator=-numberator;
      denominator=-denominator;
    }
  }
  int getNumberator() {
    return numberator;
  }
  int getDenominator() {
    return denominator;
  }
  int f(int a,int b){
    if(a==0) return 1;
    if(a<b) {
      int c=a;
      a=b;
      b=c;
    }
    int r=a%b;
    while(r!=0) {
      a=b;
      b=r;
      r=a%b;
    }
    return b;
  }
  Rational add(Rational r) {
    int a=r.getNumberator();
    int b=r.getDenominator();
    int newNumberator=numberator*b+denominator*a;
    int newDenominator=denominator*b;
    Rational result=new Rational();
    result.setNumberator(newNumberator);
    result.setDenominator(newDenominator);
    return result;
  }
  Rational sub(Rational r) {
    int a=r.getNumberator();
    int b=r.getDenominator();
    int newNumberator=numberator*b-denominator*a;
    int newDenominator=denominator*b;
    Rational result=new Rational();
    result.setNumberator(newNumberator);
    result.setDenominator(newDenominator);
    return result;
  }
  Rational muti(Rational r) {
    int a=r.getNumberator();
    int b=r.getDenominator();
    int newNumberator=numberator*a;
    int newDenominator=denominator*b;
    Rational result=new Rational();
    result.setNumberator(newNumberator);
    result.setDenominator(newDenominator);
    return result;
  }
  Rational div(Rational r) {
    int a=r.getNumberator();
    int b=r.getDenominator();
    int newNumberator=numberator*b;
    int newDenominator=denominator*a;
    Rational result=new Rational();
    result.setNumberator(newNumberator);
    result.setDenominator(newDenominator);
    return result;
  }
}
