import java.util.*;
class StudentKey implements Comparable { 
   double d=0; 
   StudentKey (double d) {
     this.d=d;
   }
   public int compareTo(Object b) {
     StudentKey st=(StudentKey)b;
     if((this.d-st.d)==0)
        return -1;
     else
        return (int)((this.d-st.d)*1000);
  }
}
class Student2 {
    String name=null;
    double math,english;
    Student2(String s,double m,double e) {
       name=s; 
       math=m;
       english=e;
    }
}
public class Example15_9 {
   public static void main(String args[ ]) {
      TreeMap<StudentKey,Student2>  treemap= new TreeMap<StudentKey,Student2>();
      String str[]={"赵一","钱二","孙三","李四"};
      double math[]={89,45,78,76};
      double english[]={67,66,90,56};
      Student2 student[]=new Student2[4];
      for(int k=0;k<student.length;k++) {
         student[k]=new Student2(str[k],math[k],english[k]);
      }
      StudentKey key[]=new StudentKey[4] ;
      for(int k=0;k<key.length;k++) {
         key[k]=new StudentKey(student[k].math); //关键字按数学成绩排列大小
      }
      for(int k=0;k<student.length;k++) {
         treemap.put(key[k],student[k]);
      }
      int number=treemap.size();
      System.out.println("树映射中有"+number+"个对象,按数学成绩排序:");
      Collection<Student2> collection=treemap.values();
      Iterator<Student2> iter=collection.iterator();
      while(iter.hasNext()) {
         Student2 stu=iter.next();
         System.out.println("姓名 "+stu.name+" 数学 "+stu.math);
      }
      treemap.clear();
      for(int k=0;k<key.length;k++) {
         key[k]=new StudentKey(student[k].english);//关键字按英语成绩排列大小
      }
      for(int k=0;k<student.length;k++) {
         treemap.put(key[k],student[k]);
      }
      number=treemap.size();
      System.out.println("树映射中有"+number+"个对象:按英语成绩排序:");
      collection=treemap.values();
      iter=collection.iterator();
      while(iter.hasNext()) {
         Student2 stu=(Student2)iter.next();
         System.out.println("姓名 "+stu.name+" 英语 "+stu.english);
      }
    }
}
