package sizeyunsuan;
import java.io.*;
import java.util.StringTokenizer;
public class Jieguo extends Biaodashi {
    public Jieguo() {
        super();
    }
    public void inFile(String fileName) throws IOException {
        String Ti = "";
        int patnum = 0;
        BufferedWriter bf = new BufferedWriter(new FileWriter(fileName));
        for (String i : list) {
            patnum++;
            bf.write("第" + patnum + "题");
            bf.newLine();
            Ti = i + "= ";
            bf.write(Ti);
            bf.newLine();
            bf.newLine();
        }
        bf.close();
    }
    //将结果写入文件
    public void outFile(String fileName1, String fileName2) throws IOException {
        Fuhao infixToSuffix = new Fuhao();
        Jisuan cal = new Jisuan();
        BufferedReader br = new BufferedReader(new FileReader(fileName1));
        BufferedWriter bf = new BufferedWriter(new FileWriter(fileName2));
        while (true) {
            String tmp = br.readLine();
            if (tmp != null) {
                if (tmp.indexOf("=") != -1) {
                    ExpreNum++;
                    StringTokenizer tokenizer = new StringTokenizer(tmp, "=");
                    infixToSuffix.evaluate(tokenizer.nextToken());
                    list3.add(tmp);
                    if ((" " + cal.evaluate(infixToSuffix.getMessage())).equals(tokenizer.nextToken())) {
                        list3.add("正确");
                        trues++;
                    } else {
                        list3.add("错误");
                        list3.add("正确答案为" + cal.evaluate(infixToSuffix.getMessage()));
                    }
                } else list3.add(tmp);
            } else break;
        }
        list3.add("总题数" + ExpreNum);
        list3.add("正确总题数" + trues);
        double accuracy = (double) trues / ExpreNum;
        list3.add("正确率" + nf.format(accuracy));
        for (String i : list3) {
            bf.write(i);
            bf.newLine();
        }
        br.close();
        bf.close();
    }
}
