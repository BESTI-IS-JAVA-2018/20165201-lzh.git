<RelativeLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:paddingLeft="2dp"
    android:paddingRight="2dp">
    <Button
        android:id="@+id/cancelButton"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="20165201"
        android:layout_marginTop="70dp"
        android:layout_alignParentTop="true"
        android:layout_centerHorizontal="true" />
    <Button
        android:id="@+id/saveButton"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="李梓豪"
        android:layout_below="@+id/cancelButton"
        android:layout_alignLeft="@+id/cancelButton"
        android:layout_alignStart="@+id/cancelButton"
        android:layout_marginTop="23dp" />
    <ImageView
        android:layout_width="150dp"
        android:layout_height="150dp"
        android:layout_marginTop="50dp"
        android:padding="4dp"
        android:src="@android:drawable/ic_dialog_email"
        android:id="@+id/imageView"
        android:layout_below="@+id/saveButton"
        android:layout_centerHorizontal="true" />
    <LinearLayout
        android:id="@+id/filter_button_container"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_alignParentBottom="true"
        android:gravity="center|bottom"
        android:background="@android:color/white"
        android:orientation="horizontal" >
        <Button
            android:id="@+id/filterButton"
            android:layout_width="wrap_content"
            android:layout_height="fill_parent"
            android:layout_marginTop="10dp"
            android:text="Filter" />
        <Button
            android:id="@+id/shareButton"
            android:layout_width="wrap_content"
            android:layout_height="fill_parent"
            android:layout_marginTop="20dp"
            android:text="Share" />
        <Button
            android:id="@+id/deleteButton"
            android:layout_width="wrap_content"
            android:layout_height="fill_parent"
            android:layout_marginTop="30dp"
            android:text="Delete" />
    </LinearLayout>
</RelativeLayout>
